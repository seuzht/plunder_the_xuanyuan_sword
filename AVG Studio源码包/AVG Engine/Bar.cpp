//bar.cpp

#include "stdafx.h"


//
CBar::CBar()
{
};
//
CBar::~CBar()
{
	DeleteDC( hMemDC );
};
//
void CBar::Reset()
{
	nDelay = 100;
	nState = 1;
}
//
BOOL CBar::InitBar(HWND hwnd,CGameData* pD,CMidi* pM)
{
	pDB = pD;
	pMidi = pM;
	HDC hdc = ::GetDC( hwnd );
	hMemDC = ::CreateCompatibleDC( hdc );
	HBITMAP hBmp = (HBITMAP)LoadImage( NULL, "bar.bmp", IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE );
	DeleteObject( ::SelectObject(hMemDC,hBmp) );
	::ReleaseDC( hwnd, hdc);
	nState    = 1;		//初始化之后就可以开始了。敌人在Bar之前初始化
	bFire     = FALSE;
	nEnemyNum = 0;
	nDelay    = 0;
	bf.AlphaFormat = 0;
	bf.BlendFlags = 0;
	bf.BlendOp = AC_SRC_OVER;
	bf.SourceConstantAlpha = 60;
	nDelay    = 100;
	return TRUE;
};
//
void CBar::Paint(HWND hwnd,HDC hdc)
{
	int blood = 0;
	//首先检测玩家是否还活着
	if( nState>0 && nState<3 )
	{
		blood = pDB->Get( 2 );
		if( blood == 0 )
		{
			pMidi->Play( hwnd, "gameover.mid" );
			nState = 3;
		}
	}
	//绘制
	if( nState != 0 )
	{	//战斗状态
		//1、绘制Blood文字、Enemies文字
		TransBlt( hdc,20,463,57,12,hMemDC,315,118,colorkey );
		TransBlt( hdc,5,445,72,12,hMemDC,315,130,colorkey );
		//2、绘制血条
		StretchBlt( hdc,81,464,300,10,hMemDC,282,132,1,10,SRCCOPY );
		StretchBlt( hdc,81,464,blood,10,hMemDC,277,132,1,10,SRCCOPY );
		//3、根据nEnemyNum绘制敌人图标
		int nLine1 = 0;
		int nLine2 = 0;
		if( nEnemyNum > 20 )
		{
			nLine2 = 20;
			nLine1 = nEnemyNum%20;
		}
		else
		{
			nLine2 = nEnemyNum;
		}
		int i;
		for( i=0; i<nLine1; i++ )
		{
			int x = 81+19*i;
			TransBlt( hdc,x,410,18,24,hMemDC,297,118,colorkey );
		}
		for( i=0; i<nLine2; i++ )
		{
			int x = 81+19*i;
			TransBlt( hdc,x,434,18,24,hMemDC,297,118,colorkey );
		}
		//4、根据bFire状态绘制抢手（如果nState==3死了，则不再绘制抢手）
		if( nState==1 || nState==2 )
		{
			if( bFire )
			{
				TransBlt( hdc,497,338,143,142,hMemDC,0,0,colorkey );
				bFire = FALSE;
			}
			else
			{
				TransBlt( hdc,515,360,125,120,hMemDC,152,22,colorkey );
			}
		}
	}
	if( nState == 2 )
	{	//战斗胜利
		//除了正常绘制的部分以外，还要绘制“Congratulations !!!”
		TransBlt( hdc,213,300,214,22,hMemDC,277,0,colorkey );
		//判断延时，然后修改状态
		if( nDelay > 0 )
			nDelay --;
		else
			nState = 0;
	}
	if( nState == 3 )
	{	//战斗失败
		//绘制“Game Over !”
//		AlphaBlend( hdc,0,0,640,480,hMemDC,287,132,1,1,bf );
		TransBlt( hdc,222,200,195,25,hMemDC,277,22,colorkey );
	}
};//Paint()函数结束
//
int CBar::SetEnemyNum(int n)
{
	int temp = nEnemyNum;
	nEnemyNum = n;
	if( nState==1 && n==0 )
	{	//战斗胜利！
		pMidi->Stop();
		pMidi->PlayWav( "victory.wav" );
		nState = 2;
	}
	return temp;
};
//
void CBar::OnShot()
{
	if( nState == 1 )
	{
		//射击状态，先发出枪响
		pMidi->PlayWav( "shoot.wav" );
		//设定打枪标志，以改变抢手图片
		bFire = TRUE;
	}
};
//
BOOL CBar::IsFight()
{
	if( nState == 1 )
		return TRUE;
	else
		return FALSE;
};
//
BOOL CBar::IsRun()
{
	if( nState == 0 )
	{
		return FALSE;
	}
	else
	{
		return TRUE;
	}
};
//the end.