//ds.cpp

#include "stdafx.h"


//
CDS::CDS()
{
};
//
CDS::~CDS()
{
	DeleteDC( m_hBufDC );
};
//
BOOL CDS::Init(HWND hwnd,CMidi* pMidi,CGameData* pDB)
{
	HDC hdc = ::GetDC( hwnd );
	m_hBufDC = ::CreateCompatibleDC( hdc );
	HBITMAP hbmp = ::CreateCompatibleBitmap( hdc,640,480 );
	DeleteObject( ::SelectObject(m_hBufDC,hbmp) );
	::ReleaseDC( hwnd, hdc);
	m_pMidi = pMidi;
	m_pDB	= pDB;
	m_BkBmp.InitBmp( hwnd );
	for( int i=0; i<10; i++ )
	{
		m_Bmp[i].InitBmp( hwnd );
	}
	m_Dlg.InitDlg( hwnd, pDB, pMidi );
	return TRUE;
};
//
void CDS::OnLButtonDown(int tx,int ty)
{
	m_Dlg.OnLButtonDown( tx, ty );
};
//
void CDS::OnTimerPrc(HWND hwnd)
{
	//1、把缓冲涂黑
	BitBlt( m_hBufDC,0,0,640,480,m_hBufDC,0,0,BLACKNESS );
	//2、绘制背景
	m_BkBmp.Paint( hwnd, m_hBufDC );
	//3、绘制所有的图片（按照顺序）
	int i;
	for( i=0; i<10; i++ )
	{
		m_Bmp[i].Paint( hwnd, m_hBufDC );
	}
	//4、绘制对话框
	m_Dlg.Paint( hwnd, m_hBufDC );
	//5、显示主缓冲
	HDC hdc = ::GetDC( hwnd );
	BitBlt( hdc,0,0,640,480,m_hBufDC,0,0,SRCCOPY );
	::ReleaseDC( hwnd, hdc);
	//6、根据忙状态判断是否可以执行下一条指令
	if( m_BkBmp.IsBusy() )
		return;
	for( i=0; i<10; i++ )
	{
		if( m_Bmp[i].IsBusy() )
			return;
	}
	if( m_Dlg.IsBusy() )
		return;
	//7、不忙的话，如果指令序列未执行完，则执行指令序列。
	//如果指令序列执行完了，则调用CluAll执行全部计算序列，然后修改系统状态。
	ACT act;
	if( GetNextAct(&act) )
	{
		if( !OnCode(hwnd,&act) )
		{
			KillTimer(hwnd,1000);
			MessageBox(hwnd,"动作指令错误！请检查对话场脚本文件动作段的正确性。",NULL,MB_ICONERROR|MB_OK);
			::PostQuitMessage( WM_QUIT );
		}
	}
	else
	{
		//指令序列执行完了，去执行CluAll
		if( !CluAll(m_pDB) )
		{
			KillTimer(hwnd,1000);
			MessageBox(hwnd,"运算指令错误！请检查对话场脚本文件运算段的正确性。",NULL,MB_ICONERROR|MB_OK);
			::PostQuitMessage( WM_QUIT );
		}
		//修改当前系统，表示对话场已经执行完
		m_pDB->SetCurSystem( 0 );
	}
};//OnTimerPrc()函数结束
//
BOOL CDS::OnCode(HWND hwnd,ACT* pAct)
{
	int n = pAct->nActType;
	if( n<1 || n>8 )
		return FALSE;
	char fname[255];
	switch( n )
	{
	case 1:
		itoa( (pAct->nPara1), fname, 10 );
		strcat( fname, ".bmp" );
		m_BkBmp.Open( fname,0,0,640,480 );
		break;
	case 2:
		itoa( (pAct->nPara1), fname, 10 );
		strcat( fname, ".mid" );
		m_pMidi->Play( hwnd, fname );
		break;
	case 3:
		m_pMidi->Stop();
		break;
	case 4:
		itoa( (pAct->nPara1), fname, 10 );
		strcat( fname, ".wav" );
		m_pMidi->PlayWav( fname );
		break;
	case 5:
		itoa( (pAct->nPara2), fname, 10 );
		strcat( fname, ".bmp" );
		m_Bmp[pAct->nPara1].Open( fname,(pAct->nPara3),(pAct->nPara4),(pAct->nPara5),(pAct->nPara6) );
		break;
	case 6:
		m_Bmp[pAct->nPara1].Close();
		break;
	case 7:
		itoa( (pAct->nPara1), fname, 10 );
		strcat( fname, ".txt" );
		m_Dlg.Open( fname,(pAct->nPara2),(pAct->nPara3) );
		break;
	case 8:
		Sleep( pAct->nPara1 );
		break;
	}
	return TRUE;
};
//the end.