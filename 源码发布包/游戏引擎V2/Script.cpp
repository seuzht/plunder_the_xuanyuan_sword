// Script.cpp

#include "stdafx.h"
#include "Game.h"

CScript::CScript()
{
	m_strScript.Empty();
}

CScript::~CScript()
{
}

// 打开文件，读出脚本
BOOL CScript::OpenScript( int id )
{
	// 构造文件名
	char psFileName[256];
	sprintf( psFileName, "Spt/%d.spt", id );

	// 读出磁盘文件
	HANDLE hFile;
	hFile = CreateFile(psFileName,GENERIC_READ,FILE_SHARE_READ,0,OPEN_EXISTING,0,NULL);
	if(hFile==INVALID_HANDLE_VALUE)
		return FALSE;

	// 将文件打开到一个CString中
	DWORD dwFileLength = GetFileSize (hFile, 0);
	char* pFile = new char[dwFileLength+1];
	memset (pFile, 0x0, dwFileLength+1);

	DWORD dwActualSize = 0;
	BOOL bReadSt;
	bReadSt = ReadFile (hFile, pFile, dwFileLength, &dwActualSize, NULL);
	CloseHandle (hFile);
	
	if (!bReadSt)
		return FALSE;
	if (dwActualSize != dwFileLength)
		return FALSE;

	m_strScript = pFile;
	delete pFile;

	return TRUE;
}

// 取出一行命令
BOOL CScript::PopCommand( CString& strCommand )
{
	if( !GetLine( strCommand, m_strScript, 0 ) )
		return FALSE;

	int nFirstLine = m_strScript.Find( '\n' );
	if( nFirstLine == -1 )
		return FALSE;

	// 删除已经读出的脚本
	m_strScript = m_strScript.Right( m_strScript.GetLength() - nFirstLine - 1 );

	return TRUE;
}

/* END */