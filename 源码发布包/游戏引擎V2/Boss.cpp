// Boss.cpp

#include "StdAfx.h"
#include "Game.h"

CBoss::CBoss( CGameMap* pGameMap, int x, int y, int nEvent )
	  :CGameObject( pGameMap )
{
	m_nEvent = nEvent;
	m_x = x;
	m_y = y;
	m_w = 32;
	m_h = 32;
	m_hDeadDC  = pGameMap->m_hNpcDeadDC;

	// 打开图片
	InitImage( FILENAME_IMG_BOSS );

	m_nLife = 10000;
	m_nStatus = BOSS_READY;
	m_nActCount = 20;
	m_nOffset_x = 0;
	m_nOffset_y = 0;
}

CBoss::~CBoss()
{
}

// 绘制
void CBoss::Draw( HDC hDC )
{
	int nLeft = m_pGameMap->m_nLeft;
	int nTop  = m_pGameMap->m_nTop;
	int x  = m_x - nLeft - 64;
	int y  = m_y - nTop  - 64;
	int sy = (m_nStatus - 1) * 160;
	
	// 绘制死亡动画
	if( m_nStatus >= BOSS_DEAD )
	{
		TransBlt( hDC, x+25, y+36, 110, 60, m_hDeadDC, 0, 60*(m_nStatus-BOSS_DEAD), COLORKEY );
		m_nActCount --;
		if( m_nActCount == 0 )
		{
			m_nActCount = 3;
			if( m_nStatus < BOSS_DEAD+7 )
				m_nStatus ++;
			else
			{
				if( m_nStatus == BOSS_DEAD+7 )
				{
					// 发出对应消息，调用后继脚本（只发一次）
					m_pGameMap->m_pMainFrm->PostMessage( ID_GAME_MESSAGE, m_nEvent, 0 );
					m_nStatus ++;
				}
			}
		}
		return;
	}

	TransBlt( hDC, x, y, 160, 160, m_hImageDC, 0, sy, COLORKEY );

	// 显示血条
	// 绘制NPC头上的血格
	SetTextColor( hDC, RGB(0,0,0) );
	TextOut( hDC, 79, 20, "司徒：", 6 );
	TextOut( hDC, 81, 20, "司徒：", 6 );
	TextOut( hDC, 80, 19, "司徒：", 6 );
	TextOut( hDC, 80, 21, "司徒：", 6 );
	SetTextColor( hDC, RGB(255,255,0) );
	TextOut( hDC, 80, 20, "司徒：", 6 );
	BitBlt( hDC, 123, 23, 402, 10, m_hImageDC, 0, 0, WHITENESS );
	BitBlt( hDC, 124, 24, 400, 8,  m_hImageDC, 0, 0, BLACKNESS );
	StretchBlt(hDC,124,24,m_nLife*400/10000,8,m_hImageDC,0,0,1,1,SRCCOPY);
}

// 移动
void CBoss::Move( long lNow )
{
	if( m_nStatus >= BOSS_DEAD )
		return;

	// 1>动画状态的推移
	// 攻击结束则准备，准备结束则攻击，无限循环
	m_nActCount --;

	if( m_nActCount == 0 )
	{
		if( m_nStatus == BOSS_READY )
		{
			// 转入攻击动作
			// 播放音效
			m_pGameMap->m_pMainFrm->m_pMidi->PlayWave( FILENAME_WAV_BOSS_KILL );
			m_nStatus = BOSS_ATTACK;
			m_nActCount = 50;
			// 计算攻击动作的运动偏移量
			CalcOffset();
		}
		else if( m_nStatus >= BOSS_ATTACK )
		{
			// 转入准备状态
			m_nStatus = BOSS_READY;
			m_nActCount = 20;
		}
	}

	// 改变图片
	if( m_nStatus >= BOSS_ATTACK )
	{
		m_nStatus ++;
		if( m_nStatus == 6 )
			m_nStatus = BOSS_ATTACK;
	}

	// 2>位置的推移
	int nOld_x = m_x;
	int nOld_y = m_y;

	// 判断X方向是否可以行走
	m_x += m_nOffset_x;
	if( (m_pGameMap->HitWallTest(this)) )
		m_x = nOld_x;

	// 判断Y方向是否可以行走
	m_y += m_nOffset_y;
	if( (m_pGameMap->HitWallTest(this)) )
		m_y = nOld_y;

	// 3>只要不是准备状态，BOSS将不断地攻击角色
	if( m_nStatus > 1 )
		Fire();
}

// 计算攻击动作的运动偏移量
void CBoss::CalcOffset()
{
	// 1>得到与主角的相对位置
	int nHeroCenter_x = m_pGameMap->m_pHero->m_x + HERO_CENTER_OFFSET_X;
	int nHeroCenter_y = m_pGameMap->m_pHero->m_y + HERO_CENTER_OFFSET_Y;

	int nCenter_x = m_x + 16;
	int nCenter_y = m_y + 16;

	int dx = nHeroCenter_x - nCenter_x;
	int dy = nHeroCenter_y - nCenter_y;

	// 2>偏移量的平方和应保持在400左右（BOSS运动速度要比角色快一些）
	int nSpeed = 400;
	int i = 100;
	if( (dx * dx + dy * dy) > nSpeed )
	{
		do
		{
			m_nOffset_x = dx * i / 100;
			m_nOffset_y = dy * i / 100;
			i --;
		}
		while( (m_nOffset_x * m_nOffset_x + m_nOffset_y * m_nOffset_y) >= nSpeed );
	}
	else
	{
		do
		{
			m_nOffset_x = dx * i / 100;
			m_nOffset_y = dy * i / 100;
			i ++;
		}
		while( (m_nOffset_x * m_nOffset_x + m_nOffset_y * m_nOffset_y) <= nSpeed );
	}
}

// 判断自己是否可以被显示
BOOL CBoss::CanBeShown()
{
	return TRUE;
}

// 得到碰撞检测矩形
BOOL CBoss::GetHitTestRect( int* x, int* y, int* w, int* h )
{
	if( m_nStatus == BOSS_DEAD )
		return FALSE;

	*x = m_x;
	*y = m_y-16;
	*w = m_w;
	*h = m_h+16;
	return TRUE;
}

// 获得所在格子的坐标。每个精灵只能占一个格子的位置
// 得到以格子为单位的坐标系统（用于地图绘制和碰撞检测）
BOOL CBoss::GetGride( int* pX, int* pY )
{
	int iCenterX = m_x + 16;
	int iCenterY = m_y + 16;

	if( (iCenterX < 0) || (iCenterX > TILE_W * GRIDE_W) )
		return FALSE;
	if( (iCenterY < 0) || (iCenterY > TILE_H * GRIDE_H) )
		return FALSE;

	*pX = iCenterX / TILE_W;
	*pY = iCenterY / TILE_H;
	return TRUE;
}

// 攻击角色
void CBoss::Fire()
{
	int X1, Y1, X2, Y2;
	if( !m_pGameMap->m_pHero->GetGride( &X1, &Y1 ) )
		return;

	if( !GetGride( &X2, &Y2 ) )
		return;
	
	int dX = abs( X1 - X2 );
	int dY = abs( Y1 - Y2 );
	if( (dX < 3) && (dY < 3) )
	{
		int nPower = g_pnBossInjure[dX][dY];
		m_pGameMap->HitHero( nPower / 3 );
	}
}

// 遭受攻击
void CBoss::BeHit( int nPower )
{
	if( m_nLife == 0 )
		return;

	int nInjure = nPower / 100;

	m_nLife -= nInjure;

	// 死亡处理
	if( m_nLife <= 0 )
	{
		m_nLife = 0;
		m_nStatus = BOSS_DEAD;
		m_nActCount = 3;

		// 调用GameMap的函数给角色增加经验值
		m_pGameMap->AddExp( BOSS_EXP );
	}
}

/* END */