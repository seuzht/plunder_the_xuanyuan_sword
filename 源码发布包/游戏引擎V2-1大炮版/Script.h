// Script.h


#if !defined(__SCRIPT_H__)
#define __SCRIPT_H__

class CScript  
{
public:
	// 保存脚本的字符串
	CString m_strScript;

public:
	CScript();
	virtual ~CScript();

public:
	// 打开文件，读出脚本
	BOOL OpenScript( int id );

	// 取出一行命令
	BOOL PopCommand( CString& strCommand );
};

#endif // !defined(__SCRIPT_H__)
