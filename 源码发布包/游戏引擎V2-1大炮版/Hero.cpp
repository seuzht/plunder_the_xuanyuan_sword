// Hero.cpp: implementation of the CHero class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Game.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CHero::CHero( CGameMap* pGameMap )
	: CGameObject( pGameMap )
{
	// 死亡动画
	m_hDeadDC  = pGameMap->m_hNpcDeadDC;

	// 默认面下站立
	m_nStatus = 6;
	m_nCurFrame = 6;
	m_nStepDelay = 0;
	// 默认动作都已经完成
	m_wAct = 0xC0;
	// 默认尺寸
	m_w = HERO_W;
	m_h = HERO_H;
	// 默认属性
	m_nLife = 1000;
	m_nLevel = 50;
	m_nExp = 0;
	m_hImageDC2 = NULL;
    // 射击、更换弹夹间隔
    m_lActDelay = 100;
    m_lStartTime = 0;
}

CHero::~CHero()
{
	if( m_hImageDC2 != NULL )
	{
		::DeleteDC( m_hImageDC2 );
		m_hImageDC2 = NULL;
	}
}

// 绘制
void CHero::Draw( HDC hDC, int x, int y )
{
	int nLeft = m_pGameMap->m_nLeft;
	int nTop  = m_pGameMap->m_nTop;

	if( (m_x + m_w < nLeft) || (m_x > nLeft + GAME_AREA_W) )
		return;
	if( (m_y + m_w < nTop)  || (m_y > nTop  + GAME_AREA_H) )
		return;

	int tx = m_x - nLeft + x;
	int ty = m_y - nTop  + y;

	// 绘制死亡动画
	if( m_wAct == HERO_ACT_DEAD )
	{
		TransBlt( hDC, tx-(110-NPC_W)/2, ty-(60-NPC_H)/2, 110, 60, m_hDeadDC, 0, 60*(m_nCurFrame-100), COLORKEY );
		m_nCurFrame ++;
		if( m_nCurFrame == 109 )
			m_pGameMap->m_pMainFrm->HeroDead();
		return;
	}

	int nIndex = (m_nCurFrame % 20) - 1;
	int sx = hero_act_x[ nIndex ];
	int sy = hero_act_y[ nIndex ];
	if( m_nCurFrame > 20 )
		sy += 192;
	if( m_nCurFrame > 40 )
		sy += 192;

	// 绘制副角色
	if( m_hImageDC2 != NULL )
	{
		// 副角色总是在角色的右上方
		TransBlt( hDC, m_x-nLeft+HERO2_OFFSET_X+x, m_y-nTop+HERO2_OFFSET_Y+y,m_w, m_h, m_hImageDC2, sx, sy, COLORKEY );
	}

	// 绘制角色（角色总是在副角色的左下方，会遮挡副角色）
	TransBlt( hDC, m_x-nLeft+x,m_y-nTop+y, m_w, m_h, m_hImageDC, sx, sy, COLORKEY );
}

// 获得所在格子的坐标。每个精灵只能占一个格子的位置
// 得到以格子为单位的坐标系统（用于地图绘制和碰撞检测）
BOOL CHero::GetGride( int* pX, int* pY )
{
	int iCenterX = m_x + HERO_CENTER_OFFSET_X;
	int iCenterY = m_y + HERO_CENTER_OFFSET_Y;

	if( (iCenterX < 0) || (iCenterX > TILE_W * GRIDE_W) )
		return FALSE;
	if( (iCenterY < 0) || (iCenterY > TILE_H * GRIDE_H) )
		return FALSE;

	*pX = iCenterX / TILE_W;
	*pY = iCenterY / TILE_H;
	return TRUE;
}

// 得到碰撞检测矩形
BOOL CHero::GetHitRect( int* px, int* py, int* pw, int* ph )
{
	*px = m_x;
	*py = m_y + m_h - TILE_H;
	*pw = TILE_W;
	*ph = TILE_H;
	return TRUE;
}

// 得到地图占用矩形
BOOL CHero::GetPlaceRect( int* px, int* py, int* pw, int* ph )
{
	*px = m_x + 1;
	*py = m_y + m_h - TILE_H + 1;
	*pw = TILE_W - 2;
	*ph = TILE_H - 2;
	return TRUE;
}

// 设置动作
BOOL CHero::SetAct( WORD wAct, long lNow, long lActDelay )
{
	if( m_wAct == HERO_ACT_DEAD )
		return FALSE;

	// 动作未完成则拒绝接受新的动作
	if( (m_wAct & HERO_ACT_GO_COMPLETE) > 0 )
	{
		switch( wAct )
		{
		case HERO_ACT_GO_UP:
		case HERO_ACT_GO_DOWN:
		case HERO_ACT_GO_LEFT:
		case HERO_ACT_GO_RIGHT:
			{
				m_wAct &= 0xF0;
				m_wAct |= wAct;
				return TRUE;
			}
			break;
		}
	}

 	if( (m_wAct & HERO_ACT_OTHER_COMPLETE) > 0 )
	{
		switch( wAct )
		{
		case HERO_ACT_FIRE:
			{
                // 处理子弹射速问题，当到达时间间隔时后才能开下一枪
                if( (lNow - m_lStartTime) < m_lActDelay )
                    return FALSE;

				m_wAct &= ~HERO_ACT_RENEW_CASSETTE;
				m_wAct |= wAct;
				m_lStartTime = lNow;
                m_lActDelay = lActDelay;
				return TRUE;
			}
			break;
		case HERO_ACT_RENEW_CASSETTE:
			{
                if( (lNow - m_lStartTime) < m_lActDelay )
                    return FALSE;

				m_wAct &= ~HERO_ACT_FIRE;
				m_wAct |= wAct;
				// 关闭开火状态
				if( m_nStatus > 40 )
                {
					m_nStatus -= 20;
                    m_nCurFrame = m_nStatus;
                    m_nStepDelay = 0;
                }
				m_lStartTime = lNow;
                m_lActDelay = lActDelay;
				return TRUE;
			}
			break;
		}
	}

	return FALSE;
}

// 持枪/无枪的设置
BOOL CHero::SetGun( BOOL bHoldGun )
{
	if( bHoldGun )
	{
		if( m_nStatus < 20 )
			m_nStatus += 20;
	}
	else
	{
		if( m_nStatus > 20 )
			m_nStatus %= 20;
	}

	m_nCurFrame = m_nStatus;
	return TRUE;
}

// 测试角色手中是否有枪
BOOL CHero::HaveGun()
{
	if( m_nStatus > 20 )
		return TRUE;
	return FALSE;
}

// 测试角色的动作是否已经完成
BOOL CHero::ActComplete()
{
	return ((m_wAct & HERO_ACT_GO_COMPLETE) > 0);
}

// 设置角色的面向
BOOL CHero::SetDirection( int nDirection )
{
    int nFaceDirection = 0;
    switch( nDirection )
    {
    case HERO_DIRECTION_UP:    nFaceDirection = 3;    break;
    case HERO_DIRECTION_DOWN:  nFaceDirection = 6;    break;
    case HERO_DIRECTION_LEFT:  nFaceDirection = 9;    break;
    case HERO_DIRECTION_RIGHT: nFaceDirection = 12;   break;
    default:
        return FALSE;
    }

	m_nStatus = 20 * (m_nStatus / 20) + nFaceDirection;
	m_nCurFrame = m_nStatus;
    return TRUE;
}

// m_nCurFrame;
//  1:无枪，面上迈步1； 21:持枪，面上迈步1； 41:射击，面上迈步1；
//  2:无枪，面上迈步2； 22:持枪，面上迈步2； 42:射击，面上迈步2；
//  3:无枪，面上站立；  23:持枪，面上站立；  43:射击，面上站立；
//  4:无枪，面下迈步1； 24:持枪，面下迈步1； 44:射击，面下迈步1；
//  5:无枪，面下迈步2； 25:持枪，面下迈步2； 45:射击，面下迈步2；
//  6:无枪，面下站立；  26:持枪，面下站立；  46:射击，面下站立；
//  7:无枪，面左迈步1； 27:持枪，面左迈步1； 47:射击，面左迈步1；
//  8:无枪，面左迈步2； 28:持枪，面左迈步2； 48:射击，面左迈步2；
//  9:无枪，面左站立；  29:持枪，面左站立；  49:射击，面左站立；
// 10:无枪，面右迈步1； 30:持枪，面右迈步1； 50:射击，面右迈步1；
// 11:无枪，面右迈步2； 31:持枪，面右迈步2； 51:射击，面右迈步2；
// 12:无枪，面右站立；  32:持枪，面右站立；  52:射击，面右站立；
// 无枪动作图片的y值加192为持枪动作图片，加384为射击动作图片。
// HERO_ACT_REST            0x00              // 无动作
// HERO_ACT_GO_UP           0x01              // 向上走
// HERO_ACT_GO_DOWN         0x02              // 向下走
// HERO_ACT_GO_LEFT         0x03              // 向左走
// HERO_ACT_GO_RIGHT        0x04              // 向右走
// HERO_ACT_UNIQUE_SKILL    0x08              // 绝招
// HERO_ACT_FIRE            0x10              // 开火
// HERO_ACT_CHANGE_CASSETTE 0x20              // 更换弹夹
// HERO_ACT_GO_COMPLET      0x40              // 行走动作执行完成标志
// HERO_ACT_OTHER_COMPLETE  0x80              // 其他动作执行完成标志
// 
// 移动
void CHero::Move (long lNow, CPoint ptMouse)
{
	if( m_wAct == HERO_ACT_DEAD )
		return;

	// 将当前动作推进到下一个状态
	// 1>行走动作的推进
	if ((m_wAct & HERO_ACT_GO_COMPLETE) > 0)
	{
		WORD wAct = m_wAct & 0x07;
		// 行走状态已经结束。
		// 如果没有下一个行走动作，则设置动作为站立等待状态
		if( wAct == 0 )
		{
			while( ((m_nStatus % 20) % 3) != 0 )
				m_nStatus ++;

            m_nCurFrame = m_nStatus;
		}
		else
		{
			int nFaceDirection;
			switch( wAct )
			{
			case HERO_ACT_GO_UP:
				nFaceDirection = 3;
				break;
			case HERO_ACT_GO_DOWN:
				nFaceDirection = 6;
				break;
			case HERO_ACT_GO_LEFT:
				nFaceDirection = 9;
				break;
			case HERO_ACT_GO_RIGHT:
				nFaceDirection = 12;
				break;
			}

			// 移动到新位置
			BOOL bHitWall;
			BOOL bMoved = MoveToNewPos(wAct, &bHitWall);

            // 如果移动失败
            if (! bMoved)
            {
				// 如果是无事件撞墙引起的，首先尝试是不是可以朝角色的左右两侧移动一下
				if (bHitWall && GetAsyncKeyState(VK_LBUTTON)<0) {

					// 根据鼠标的指引方向进行尝试
					switch (wAct) {
					case HERO_ACT_GO_UP:
					case HERO_ACT_GO_DOWN:
						wAct = ptMouse.x < (GAME_AREA_W / 2) ? HERO_ACT_GO_LEFT : HERO_ACT_GO_RIGHT;
						break;
					case HERO_ACT_GO_LEFT:
					case HERO_ACT_GO_RIGHT:
						wAct = ptMouse.y < (GAME_AREA_H / 2) ? HERO_ACT_GO_UP : HERO_ACT_GO_DOWN;
						break;
					}
					bMoved = MoveToNewPos(wAct);
				}

				// 还是不能移动，设置为站立状态
				if (! bMoved)
				{
					m_nStatus = 20 * (m_nStatus / 20) + nFaceDirection;
					m_nCurFrame = m_nStatus;
					m_nStepDelay = 0;
					m_wAct &= 0xF8;
				}
            }
            else
            {
                // 如果跟上个动作的行走方向一致，则不设置CurFrame
			    if( ((m_nStatus % 20) >= nFaceDirection) ||
				    ((m_nStatus % 20) < (nFaceDirection - 2)) )
                {
                    // 设置行走动作
	    		    m_nStatus = 20 * (m_nStatus / 20) + nFaceDirection - 2;
                    m_nCurFrame = m_nStatus;
                    m_nStepDelay = 0;
                }
                else
			    {
                    // 设置行走动作
	    		    m_nStatus = 20 * (m_nStatus / 20) + nFaceDirection - 2;
                }

				// 清除动作完成标志
    			m_wAct &= ~HERO_ACT_GO_COMPLETE;
            }
		}
	}
	else
	{
        // 行走动作切换
        // 只要角色处于行走中，就要按照步子切换桢，而不能按照m_nStatus切换
		m_nStepDelay ++;
		if( m_nStepDelay == STEP_DELAY )
            m_nCurFrame +=2;
        else if( m_nStepDelay == STEP_DELAY * 2 )
            m_nCurFrame --;
        else if( m_nStepDelay == STEP_DELAY * 3 )
            m_nCurFrame ++;
        else if( m_nStepDelay == STEP_DELAY * 4 )
        {
			m_nCurFrame -=2;
            m_nStepDelay = 0;
        }

		// 推进到下一个状态
		m_nStatus ++;
		// 移动到新位置
		MoveToNewPos( m_wAct & 0x07 );
		// 执行到走动的第二步，则设置行走动作完成标志并清除动作
		if( ((m_nStatus % 20) % 3) == 2 )
		{
		    m_wAct &= 0xF8;
		    m_wAct |= HERO_ACT_GO_COMPLETE;
        }
	}

	// 2>更换弹夹和开火动作的推进
	if( (m_wAct & HERO_ACT_RENEW_CASSETTE) > 0 )
	{
		// 更换弹夹动作
		m_wAct &= ~HERO_ACT_RENEW_CASSETTE;
		m_wAct |= HERO_ACT_OTHER_COMPLETE;
	}
	else if( (m_wAct & HERO_ACT_FIRE) > 0 )
	{
		// 开火动作
		if( m_nStatus < 20 )
		{
			// 无枪状态不能开火
			m_wAct &= ~HERO_ACT_FIRE;
			m_wAct |= HERO_ACT_OTHER_COMPLETE;
		}
		else if( m_nStatus > 40 )
		{
			// 如果正在开火，则关闭开火
			m_nStatus -= 20;
            m_nCurFrame -= 20;
		    m_wAct &= ~HERO_ACT_FIRE;
 			m_wAct |= HERO_ACT_OTHER_COMPLETE;
		}
		else if( m_nStatus > 20 )
        {
			// 持枪状态转换为开火状态
			m_nStatus += 20;
            m_nCurFrame += 20;
            m_wAct &= ~HERO_ACT_OTHER_COMPLETE;
        }
	}
}

// 移动到新位置
BOOL CHero::MoveToNewPos (WORD wAct, BOOL *pbHitWall)
{
	int nOld_x = m_x;
	int nOld_y = m_y;

	// (需要增加事件检测)
	int X; int Y;
	if( GetGride( &X, &Y ) )
	{
		switch( wAct )
		{
		case HERO_ACT_GO_UP:
			{
				if( Y > 0 )
					m_y -= WALK_STEP;
			}
			break;
		case HERO_ACT_GO_DOWN:
			{
				if( Y < GRIDE_H - 1 )
					m_y += WALK_STEP;
			}
			break;
		case HERO_ACT_GO_LEFT:
			{
				if( X > 0 )
					m_x -= WALK_STEP;
			}
			break;
		case HERO_ACT_GO_RIGHT:
			{
				if( X < GRIDE_W - 1 )
					m_x += WALK_STEP;
			}
			break;
		}
	}

	BOOL bHitWallTest = m_pGameMap->HitWallTest(this);
	BOOL bEventTest   = m_pGameMap->EventTest(this);

	if( bHitWallTest || bEventTest ||
   		(m_pGameMap->HitTest(this) != NULL) )
	{
		m_x = nOld_x;
		m_y = nOld_y;
		
		if (pbHitWall && ! bEventTest) {
			*pbHitWall = TRUE;
		}
		return FALSE;
	}

	// 移动地图
	m_pGameMap->MoveMap( m_x - nOld_x, m_y - nOld_y );
	if (pbHitWall) {
		*pbHitWall = FALSE;
	}
    return TRUE;
}

void CHero::Die()
{
	m_nLife = 0;
	m_wAct = HERO_ACT_DEAD;
	m_nStatus = 100;
	m_nCurFrame = 100;
	m_pGameMap->m_pMainFrm->m_pMidi->PlayWave( FILENAME_WAV_DEAD );
}

// 初始化副角色图片设备
BOOL CHero::InitImage2( char* psFileName )
{
	// 打开位图
	HBITMAP hBmp=(HBITMAP)LoadImage(NULL,psFileName,IMAGE_BITMAP,0,0,LR_LOADFROMFILE);
	if( hBmp == NULL )
	{
		m_hImageDC2 = NULL;
		return FALSE;
	}

	HDC hdc = ::GetDC( theApp.m_pMainWnd->m_hWnd );
	m_hImageDC2 = ::CreateCompatibleDC( hdc );
	DeleteObject( ::SelectObject( m_hImageDC2, hBmp ) );
	ReleaseDC( theApp.m_pMainWnd->m_hWnd, hdc );

	return TRUE;
}

// 判断副角色是否存在
BOOL CHero::HaveHero2()
{
	if( m_hImageDC2 == NULL )
		return FALSE;

	return TRUE;
}

// END